package main

import (
	"bytes"
	"fmt"
	"log"
	"net"
)

type genericMessage struct {
    Content string `json:"content"`;
}

var connectionsList[] net.Conn;

func main() {
    ln, err := net.Listen("tcp6", ":8080")
    if (err != nil) { log.Fatal(err) }

    defer ln.Close()

    defer fmt.Println("se cerro pipipi");

    for {
        //connection is accepted
        conn, err := ln.Accept()
        if err != nil {
            log.Println(err);
        } else {
            fmt.Println("connection made with ->\t", conn.LocalAddr().String());
        }

        connectionsList = append(connectionsList, conn);

        go readSocketData(conn);
    }
}

func readSocketData(conn net.Conn) {
    //for each connection, lets say, we read each data buffer we receive
    buf := make([]byte, 1024);

    defer conn.Close();
    for {
        n, err := conn.Read(buf)
        if (err != nil) {
            log.Println(err);
            break;
        } else {
            fmt.Printf("Received data: \"%s\" from %s\n", string(buf[:n]), conn.LocalAddr().String());

            msg := bytes.NewBufferString(fmt.Sprintf("%s: %s", conn.LocalAddr().String(), string(buf[:n])));

            for _, elem := range connectionsList {
                elem.Write(msg.Bytes());
            }
        }
    }
}
